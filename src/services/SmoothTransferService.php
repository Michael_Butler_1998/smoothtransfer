<?php
/**
 * Rezi API plugin for Craft CMS 3.x
 *
 * An integration with dezrez cloud based estate agency software
 *
 * @link      https://github.com/Jegard
 * @copyright Copyright (c) 2018 Luca Jegard
 */

namespace michaelbutler\smoothtransfer\services;

use michaelbutler\smoothtransfer\SmoothTransfer;

use Craft;
use craft\base;

use craft\base\Component;
//use lucajegard\reziapi\models\RezApiModel;
use michaelbutler\smoothtransfer\records\SmoothTransferRecord;
use craft\elements\Entry;
use craft\elements\Asset;
use craft\helpers\FileHelper;
use craft\helpers\Json;

use craft\web\twig\variables\Sections;
use craft\elements\Category;

use craft\models;
use craft\models\Section;
use craft\models\Section_SiteSettings;

use craft\base\Element;

use craft\models\FieldGroup;
use craft\services\Fields;
use craft\base\FieldInterface;

// Elements Use commands
use craft\db\Query;
use craft\db\QueryAbortedException;
use craft\db\Table;


use craft\web\twig\variables;

/**
 * SmoothTransferService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Michael Butler
 * @package   Smooth Transfer
 * @since     1.0.0
 */
class SmoothTransferService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     ReziApi::$plugin->reziApiService->exampleService()
     *
     * @return mixed
     */

    //  GET information functions
    public function returnAllFields(){ 
        $fields = Craft::$app->fields->getAllFields(false);
        // https://docs.craftcms.com/api/v3/craft-base-fieldtrait.html#public-properties
        $importantFieldContent = array();
        $i = 0;
        foreach($fields as $key => $fieldsContent ){
       
            $importantFieldContent[$i]['handle'] = $fieldsContent['handle'];
            $importantFieldContent[$i]['name'] = $fieldsContent['name'];
            $importantFieldContent[$i]['fieldType'] = get_class($fieldsContent);
            $importantFieldContent[$i]['instructions'] = $fieldsContent['instructions'];
            $importantFieldContent[$i]['context'] = $fieldsContent['context'];
            $importantFieldContent[$i]['uid'] = $fieldsContent['uid'];
            $importantFieldContent[$i]['id'] = $fieldsContent['id'];
            
            $i ++;

        }
        return $importantFieldContent;
    }
    public function returnAllSections(){
        $sections = Craft::$app->sections->getAllSections();
        $importantSectionContent = array();
        $i = 0;
        foreach($sections as $sectionsContent){
            $importantSectionContent[$i]['uriFormat'] = $sectionsContent->siteSettings[1]['uriFormat'];
            $importantSectionContent[$i]['template'] = $sectionsContent->siteSettings[1]['template'];

            $importantSectionContent[$i]['name'] = $sectionsContent['name'];
            $importantSectionContent[$i]['handle'] = $sectionsContent['handle'];
            $importantSectionContent[$i]['type'] = $sectionsContent['type'];
            $importantSectionContent[$i]['uid'] = $sectionsContent['uid'];
            $importantSectionContent[$i]['id'] = $sectionsContent['id'];
            $i ++;
        }
        return $importantSectionContent;
    }
    public function returnAllEntries(){
        //https://github.com/craftcms/cms/blob/77d7e72deade11f6a3cf4099cca36da3f73cd9b3/docs/dev/element-queries/entry-queries.md
       
        // 1: Start new entries query like this..\craft\elements\Entry::find();

        // 2: add parameters with ->after(...)  // other parameters like ->limit(1)

        // 3: call it using ->all();

        $allEntries = \craft\elements\Entry::find()
            ->all();

        $entriesArray = array();
        $i = 0;
        $currentId = 0;
        foreach($allEntries as $entryContent){
            $entriesArray[$i]['sectionId'] = $entryContent['sectionId'];
            $entriesArray[$i]['id'] = $entryContent['id'];
            $currentId =  $entriesArray[$i]['id'];
            $sections = (new \yii\db\Query())
                ->select(['*'])
                ->from('sections')
                ->where(['id' => $entriesArray[$i]['sectionId'], 'dateDeleted'=>null])
                ->limit(10)
                ->all();
            
            // Finding out what fields are on the entry...
            $fields = $this->returnFieldsByEntry($entryContent);
            $fieldIdArray = $entryContent->getFieldLayout();
            if (!empty($fieldIdArray['id'])){
                $fields = $this->returnFieldsById($fieldIdArray['id']);
            }
            $x = 0;
            if (!empty($fields)){
                foreach ($fields as $individualFieldHandles){
                    $entriesArray[$i]['fields'][$x]['field handle'] = $individualFieldHandles['handle'];
                    $entriesArray[$i]['fields'][$x]['field value'] [$x] = $entryContent->getFieldValue($individualFieldHandles['handle']);
                    $x = $x + 1;
                }
            } 
            $entriesArray[$i]['sectionName'] = $sections[0]['name'];
            $entriesArray[$i]['sectionHandle'] = $sections[0]['handle'];
            $entriesArray[$i]['name'] = $sections[0]['name'];
            $entriesArray[$i]['handle'] = $sections[0]['handle'];
            $entriesArray[$i]['type'] = $sections[0]['type'];

            if ($entriesArray[$i]['type'] == 'channel' or $entriesArray[$i]['type'] == 'section'){
                $entryById = \craft\elements\Entry::find()
                    ->id($currentId)
                    ->one();
            }
           
            $i ++;
        }
        return $entriesArray;
        //here
    }
    // *** Create Functions *** //
    // ***** Fields ***** //    
    public function returnFieldTypeByID($id){
        $fieldType = (new \yii\db\Query())
            ->select(['*'])
            ->from('fields')
            ->where(['id' => $id])
            ->all();
        return $fieldType[0]['type'];
        
    }
    public function returnFieldsByEntry($entry){
        $fieldIdArray = $entry->getFieldLayout();
        if (!empty($fieldIdArray['id'])){
            
            return $this->returnFieldsById($fieldIdArray['id']);    
        }
        return;
    }
    public function returnFieldsById($fieldId){
        return Craft::$app->fields->getFieldsByLayoutId($fieldId);
    }
    public function createFieldGroup(){
        $fieldServices = Craft::$app->getFields();
        $group = new FieldGroup();
        $group->name = 'testGroup';
        $fieldServices->saveGroup($group);
    }

    public function checkFieldExistance($fieldHandle){
        $exists = false;
        $fields = Craft::$app->fields->getAllFields(false);
        $i = 0;
        foreach($fields as $fieldsContent){
            $fieldSetting = $fieldsContent[$i]['handle'];
            
            if ($fieldHandle == $fieldSetting){
                return true;
            }
        }
        return false;
    }

    public function createField($type, $name, $handle, $instructions){
        // https://docs.craftcms.com/api/v3/craft-services-fields.html#public-methods

        // 1: Get fields services initiate 
        $fieldServices = Craft::$app->getFields();

        // 2: Set the settings (minimum added below); -- see https://github.com/craftcms/cms/issues/2999 for example
        // $fieldSettings = ['type' => 'craft\fields\PlainText',
        // $fieldSettings = ['type' => 'craft\fields\Assets',
        $fieldSettings = [
            'type' => $type,
            'id'=>false,
            'groupId' => 1,
            'name' => $name,
            'handle' => $handle,
            'instructions'=> $instructions
        ];       
        $field = $fieldServices->createField($fieldSettings);

        // 3: Check for existance (get all fields and check if handle given exists already)
        $fieldExists = $this->checkFieldExistance($fieldSettings['handle']);

        // 4: If it does not, save the entry
        if (!$fieldExists){
            Craft::$app->fields->saveField($field);
        } 
        return;
    }

    // ***** Sections ***** //
    public function checkSectionExistance($sectionHandle){
        $sections = Craft::$app->sections->getAllSections();
        $i = 0;
        foreach($sections as $sectionContent){
            $sectionSetting = $sectionContent['handle'];
            
            if ($sectionHandle == $sectionSetting){
                return true;
            }
            $i ++;
        }
       return false;
    }

    public function returnSectionsFieldsByHandle($sectionHandle){
        if (!$this->checkSectionExistance($sectionHandle)){
            return 'No sections exist with this handle -- error';
        }
        //
        $currentSection = (new \yii\db\Query())
            ->select(['*'])
            ->from('entrytypes')     
            ->innerJoin('fieldlayouts', 'fieldlayouts.id=entrytypes.fieldLayoutId')       
            ->where(['entrytypes.handle' => $sectionHandle, 'entrytypes.dateDeleted'=>null])            
            ->all();
        if (!empty($currentSection[0]['fieldLayoutId'])){
            $layout = $this->returnFieldsById($currentSection[0]['fieldLayoutId']);
            // $fieldValue = Craft::$app->element->getFieldValue('testField');
            
            return $layout;
        } else {
            return '';
        }
        

    }

    // I.e. SmoothTransfer::$plugin->smoothTransferService->createSection('Section::TYPE_SINGLE', 'demo11', 'demo11', 'foo/{slug}', 'foo/_entry');
    public function createSection($type, $name, $handle, $uriFormat, $template){
        $section = new Section([
            'name' => $name,
            'handle' => $handle,
            
            'siteSettings' => [
                new Section_SiteSettings([
                    'siteId' => Craft::$app->sites->getPrimarySite()->id,
                    'enabledByDefault' => true,
                    'hasUrls' => true,
                    'uriFormat' => $uriFormat,
                    'template' => $template,
                ]),
            ]
        ]);

        if ($type == 'Section::TYPE_SINGLE'){
            $section->type=Section::TYPE_SINGLE;
        }

        $sectionExists = $this->checkSectionExistance($section->handle);
         
        if (!$sectionExists){
            $success = Craft::$app->sections->saveSection($section);
        }
       


        if ($section->type === 'Section::TYPE_STRUCTURE') {
            $section->maxLevels = 2;
        }

        // $sectionExistance = $this->checkSectionExistance($section->handle);

        // Craft::$app->getSections()->saveSection($section);
      
        return;
    }


    // ***** Entries ***** //
    public function checkEntryExistanceByHandle($entryPassedHandle){
        $entries = $this->returnAllEntries();
        
        $i = 0;
     
        foreach($entries as $entriesContent){
            $entryhold = $entriesContent;
            $fieldSetting = $entriesContent['handle'];
            
            if ($entryPassedHandle == $fieldSetting){
                return true;
            }
            $i ++;
        }            
        return false;
    }
    public function checkEntryExistanceBySlug($slug, $sectionChannelHandle){
        $entry = \craft\elements\Entry::find()
            ->slug(\craft\helpers\Db::escapeParam($slug))
            ->one();
        if ($entry){
            if ($sectionChannelHandle !== ''){
                $sectionID = $entry['sectionId'];
                $sections = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('sections')
                    ->where(['id' => $sectionID, 'dateDeleted'=>null])
                    ->limit(10)
                    ->all();
                $returnedSectionHandle = $sections[0]['handle'];
                if ($sectionChannelHandle == $returnedSectionHandle){
                    return true;
                } else {
                    return false;
                }
            }
           
            return true;
        } else {
            return false;
        }
    }
   public function returnAllEntriesWithinSection($sectionChannelHandle){
    //https://docs.craftcms.com/v3/dev/element-queries/#executing-element-queries
    return $entries = Entry::find()
        ->section($sectionChannelHandle)
        ->all();
   }
    //Handle of the section, 
    //I.E. 'news', 'Test Creation', 'test-create'
    public function createEntry($sectionHandle, $title, $slug){
        if ($this->checkEntryExistanceBySlug($slug, $sectionHandle) ) {
            return false;
        }
        $section = Craft::$app->sections->getSectionByHandle($sectionHandle);
        $entryTypes = $section->getEntryTypes();
        $entryType = reset($entryTypes);
        $entry = new Entry([
          'sectionId' => $section->id,
          'typeId' => $entryType->id,
          'fieldLayoutId' => $entryType->fieldLayoutId,
          'authorId' => 1,
          'title' => $title,
          'slug' => $slug,
          'postDate' => new \DateTime(),
        ]);
      
        return $success = Craft::$app->elements->saveElement($entry);
    }
    public function userDownloadJson($json){
        file_put_contents(__DIR__.'/userDownloadJson.json', json_encode($json));
    }   

    public function exportJsonAll(){
        //Fields - export all fields passed to it one at a time.
        //adding these into a JSON string and returning it
        $fieldsExportJson = $this->jsonExportField( $this->returnAllFields());
        $sectionsExportJson = $this->jsonExportSection();
        $fieldsAndSectionsExportJson = array_merge($fieldsExportJson, $sectionsExportJson);
        file_put_contents(__DIR__.'/fieldsAndSectionsTest.json', json_encode($fieldsAndSectionsExportJson));
        file_put_contents(__DIR__.'/fieldsTest.json', json_encode($fieldsExportJson));        
        file_put_contents(__DIR__.'/sectionsTest.json', json_encode($sectionsExportJson));        
        return 'failed - not exported all yet';
    }
    public function jsonExportField($allFields){
        $fieldObject = array('fields'=>array());
        foreach($fieldObject as $key => $allFieldsCurrent){
            if ($allFieldsCurrent = 'fields'){
                foreach($allFields as $key => $fieldToAdd){
                    $fieldObject[$allFieldsCurrent][$key]['type'] = $fieldToAdd['fieldType'];
                    $fieldObject[$allFieldsCurrent][$key]['name'] = $fieldToAdd['name'];
                    $fieldObject[$allFieldsCurrent][$key]['handle'] = $fieldToAdd['handle'];
                    $fieldObject[$allFieldsCurrent][$key]['instructions'] = $fieldToAdd['instructions'];
                }
            }
        }         
        
        return $fieldObject;
    }
    public function jsonExportSection(){
        $allSections = $this->returnAllSections();
        //createSection($type, $name, $handle, $uriFormat, $template){
        $sectionObject = array('sections'=>array()); 
        foreach($sectionObject as $key => $allSectionsCurrent){
            if ($allSectionsCurrent = 'sections'){
                foreach($allSections as $key => $sectionToAdd){
                    $sectionObject[$allSectionsCurrent][$key]['type'] = $sectionToAdd['type'];
                    $sectionObject[$allSectionsCurrent][$key]['name'] = $sectionToAdd['name'];
                    $sectionObject[$allSectionsCurrent][$key]['handle'] = $sectionToAdd['handle'];
                    $sectionObject[$allSectionsCurrent][$key]['uriFormat'] = $sectionToAdd['uriFormat'];
                    $sectionObject[$allSectionsCurrent][$key]['template'] = $sectionToAdd['template'];
                }
            }
        }   
             
        return $sectionObject;
    }
}
