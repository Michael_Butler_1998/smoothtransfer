<?php




namespace michaelbutler\smoothtransfer\models;

use michaelbutler\smoothtransfer\SmoothTransfer;

use Craft;
use craft\base\Model;

class SmoothTransferJsonModel extends Model
{
    /**
     * Some model attribute
     *
     * @var string
     */
    public $collectedJSON = [];
    

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['collectedJSON', 'array'],
            // ['name', 'default', 'value' => 'Some Default'],
            // ['type', 'string', 'values' => "alcohol,mixer,other"],
        ];
    }
}