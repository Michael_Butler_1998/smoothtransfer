<?php
/**
 * smooth transfer plugin for Craft CMS 3.x
 *
 * smooth transfer
 *
 * @link      ipopdigital.com
 * @copyright Copyright (c) 2019 Michael Butler
 */

/**
 * smooth transfer en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('smooth-transfer', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Michael Butler
 * @package   SmoothTransfer
 * @since     1.0.0
 */
return [
    'smooth transfer plugin loaded' => 'smooth transfer plugin loaded',
];
