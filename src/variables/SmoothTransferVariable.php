<?php
/**
 * Rezi Api plugin for Craft CMS 3.x
 *
 * rg
 *
 * @link      https://pluginfactory.io/
 * @copyright Copyright (c) 2018 lucajegard
 */

namespace michaelbutler\smoothtransfer\variables;

use michaelbutler\smoothtransfer\SmoothTransfer;
use craft\web\twig\variables\Sections;
use craft\models\FieldLayout;
use craft\models\EntryType;

use Craft;

/**

 */
class SmoothTransferVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param null $optional
     * @return string
     */

    
    public function returnAllFields($optional = null)
    {
        return SmoothTransfer::$plugin->smoothTransferService->returnAllFields();
    }
    public function returnAllSections($optional = null)
    {
        return SmoothTransfer::$plugin->smoothTransferService->returnAllSections();
    }
    public function returnSectionsFieldsByHandleVar($handle)
    {
        return SmoothTransfer::$plugin->smoothTransferService->returnSectionsFieldsByHandle($handle);
    }
    public function returnFieldTypeByIdVar($id)
    {
        return SmoothTransfer::$plugin->smoothTransferService->returnFieldTypeByID($id);
    }
}
