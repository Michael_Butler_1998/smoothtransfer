<?php
/**
 * Rezi API plugin for Craft CMS 3.x
 *
 * An integration with dezrez cloud based estate agency software
 *
 * @link      https://github.com/Jegard
 * @copyright Copyright (c) 2018 Luca Jegard
 */

namespace michaelbutler\smoothtransfer\controllers;

use michaelbutler\smoothtransfer\SmoothTransfer;

use Craft;
use craft\web\Controller;
use michaelbutler\smoothtransfer\models\SmoothTransferJsonModel;
use craft\elements\Entry;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Michael Butler
 * @package   SmoothTransfer
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something','update-branch'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/rezi-api/default
     *
     * @return mixed
     */
    public function actionPassjson()
    {
   
        $collectedJSONName = [];
        
        //Getting transmodel as the Json model so I can grab the collectedJSON variable.
        $transModel = new SmoothTransferJsonModel();
        //Currently not doing anything with the model - however still using it as good practice.
        $collectedJSON = $transModel->collectedJSON;
        $selected = Craft::$app->getRequest()->getRequiredParam('select');
        $type = Craft::$app->getRequest()->getRequiredParam('type');
        // Fields parameters
        $fieldName = Craft::$app->getRequest()->getRequiredParam('fieldName');
        $fieldType = Craft::$app->getRequest()->getRequiredParam('fieldType');
        $fieldContext = Craft::$app->getRequest()->getRequiredParam('fieldContext');
        $fieldUid = Craft::$app->getRequest()->getRequiredParam('fieldUid');
        $fieldId = Craft::$app->getRequest()->getRequiredParam('fieldId');
        $fieldInstructions = Craft::$app->getRequest()->getRequiredParam('fieldInstructions');
        // Sections parameters
        $sectionsName = Craft::$app->getRequest()->getRequiredParam('sectionsName');
        $sectionsHandle = Craft::$app->getRequest()->getRequiredParam('sectionsHandle');
        $sectionsType = Craft::$app->getRequest()->getRequiredParam('sectionsType');
        $sectionsUid = Craft::$app->getRequest()->getRequiredParam('sectionsUid');
        $sectionsId = Craft::$app->getRequest()->getRequiredParam('sectionsId');
        $cnt = count($type);
        $selectedCount = count($selected);



        for($i=0;$i<($cnt <= $selectedCount ? $cnt : $selectedCount);$i++){                       
            // if ($type[$i] == 'field'){       
                //If it was selected...
                for($x=0;$x<count($fieldUid);$x++){
                    if ($selected[$i] == $fieldUid[$x]) {
                        $newField = array(
                            'type'  => $type[$x],
                            'fieldName' => $fieldName[$x],
                            'fieldType' => $fieldType[$x],
                            'fieldContext' => $fieldContext[$x],
                            'fieldUid' => $fieldUid[$x],
                            'fieldId' => $fieldId[$x],
                            'fieldInstructions' => $fieldInstructions[$x],
                        );
                        $collectedJSON['field'][] = $newField;
                    }
                }                
            // } else if ($type[$i] == 'section'){
                for($y=0;$y<count($sectionsUid);$y++){
                    if ($selected[$i] == $sectionsUid[$y]) {
                        $newSection = array(
                            'type' => $type[$y],
                            'sectionsName' => $sectionsName[$y],
                            'sectionsHandle' => $sectionsHandle[$y],
                            'sectionsType' => $sectionsType[$y],
                            'sectionsUid' => $sectionsUid[$y],
                            'sectionsId' => $sectionsId[$y],
                        );
                        $collectedJSON['sections2'][] = $newSection;
                    }
                }
            // }     
        }
        SmoothTransfer::$plugin->smoothTransferService->userDownloadJson($collectedJSON);

        $json = json_encode($collectedJSON);

        header('Content-disposition: attachment; filename=SmoothTransfer.json');
        header('Content-type: application/json');

        // echo( $json);
        return json_encode($collectedJSON);
    } 
}
