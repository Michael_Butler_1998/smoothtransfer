# smooth transfer plugin for Craft CMS 3.x

smooth transfer

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require michaelbutler1998/entry-count

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for smooth transfer.

## smooth transfer Overview

-Insert text here-

## Configuring smooth transfer

-Insert text here-

## Using smooth transfer

-Insert text here-

## smooth transfer Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Michael Butler](ipopdigital.com)
